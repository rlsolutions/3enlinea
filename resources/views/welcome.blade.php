@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h1>Bienvenido!</h1>
                </div>
                <div class="card-body">
                    {{--  ***************** Initial Form with the configuration's parameters for the game *****************   --}}
                    <form id="form" action="{{ route("start") }}" method="post">
                        {{ csrf_field() }}
                        <h4>Seleccione el tipo de juego</h4>
                        <div class="float-center">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="gameKind" value="2">2 Jugadores
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="gameKind" value="pc">Jugador vs Computadora
                                </label>
                            </div>
                        </div>
                        <br><br>
                        <h4>Seleccione la figura que desea usar.</h4>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="primaryFigure" value="x">X
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="primaryFigure" value="o">O
                            </label>
                        </div>
                        <br><br>
                        <h4>Modo de guardado de partida</h4>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="saveMode" value="manual">Manual
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="saveMode" value="auto">Automatico
                            </label>
                        </div>
                        <br>
                        <button class="btn btn-primary float-right" type="submit" name="button">Empezar</button>
                    </form>
                    {{--  ***************** End Initial Form with the configuration's parameters for the game *****************   --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
