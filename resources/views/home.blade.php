@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                        {{-- ************* Check the color for by each player's turn and if there is a winner or tie *************   --}}
                        <h1 class="badge {{ $player == 1 ? 'badge-info' : 'badge-danger' }}" style="font-size: 30px;">
                            @if (empty($winner))
                                {{ 'Turno del Jugador ' .  $player }}
                            @elseif ($winner === '-')
                                ¡Empate!
                            @else
                             {{ ' ¡Felicidades jugador ' . $player . '!'}}
                            @endif
                        </h1>
                        {{-- ************* End Check the color for by each player's turn and if there is a winner or tie *************   --}}
                </div>
                {{-- ************* Here the board is painted inside a form  ************* --}}
                <div class="card-body">
                {{--  if there isen't winner the board is painted --}}
                    @if (empty ($winner))
                        <form id="form" action="{{ route("update") }}" method="post">
                            {{ csrf_field() }}
                            {{--  The logic of game, consist in some variables that travel to controller and it's get back to front --}}
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="player" value="{{ $player }}">
                            <input type="hidden" name="primaryFigure" value="{{ $primaryFigure }}">
                            <input type="hidden" name="secundaryFigure" value="{{ $secundaryFigure }}">
                            <input type="hidden" name="gameKind" value="{{ $gameKind }}">
                            <input type="hidden" name="saveMode" value="{{ $saveMode }}">

                            <div class="row col-md-12" style="background-color: grey;">
                                {{--  Foreach that paint the board   --}}
                                @foreach ($board as $key => $value)
                                    {{--  Here is created a input hidden by each board's cell with the value from controller  --}}
                                    <input type="hidden" id="{{ $key }}" name="cell[]" value="{{ $value }}">
                                    {{--  if the value is 0 then a white picture image is painted  --}}
                                    @if ($value == 0)
                                        <img class="col-md-4" src="{{ asset('/images/white.png') }}" alt="white" onclick="getVal({{ $key }}, {{ $player }})">
                                    @else
                                        {{--  if the value is 1, is de player 1 else is player 2                                   --}}
                                        @if ($value == 1)
                                            <img class="col-md-4" src="{{ asset('/images/' . $primaryFigure . '.png') }} " alt="{{ $primaryFigure }}">
                                        @else
                                            <img class="col-md-4" src="{{ asset('/images/' . $secundaryFigure . '.png')  }}" alt="{{ $secundaryFigure }}">
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </form>
                    {{--   if the winner variable is '-', is tie and tie image is painted    --}}
                    @elseif ($winner === '-')
                        <img class="col align-self-center" src="/images/empate.jpg" alt="winner">
                        {{--   if the winner variable is true, there is a winner and winner image is painted    --}}
                    @else
                        <img class="col align-self-center" src="/images/winner.jpg" alt="empate">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <a href="{{ route("/") }}" class="btn btn-warning col-md-12">Nueva Partida</a>
                <button id="btnLoad" type="button" class="btn btn-info col-md-12">Cargar Partida</button>
                <button id="btnSave" type="button" class="btn btn-success col-md-12">Guardar</button>
            </div>
        </div>
    </div>

</div>
@endsection
{{-- JQuery import from CDN for this test  --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        // detect if the save button is clicked and change the default url for save and send the form
        $("#btnSave").click(function() {
            $("#form").attr("action", "save");
            $("#form").submit();
        });

        // detect if the load button is clicked and change the default url for save and send the form
        $("#btnLoad").click(function() {
            $("#form").attr("action", "load");
            $("#form").submit();
        });
    });

    function getVal(cell, player) {
        $("#" + cell).val(player);
        $("#form").submit();
    }
</script>
