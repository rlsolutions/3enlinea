<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = [
       'board', "player", "primary_figure", 'secundary_figure',
       'gameKind', 'saveMode'
    ];

    /**
     * Board Model (Entity) winnerCheck
     * This method check if some player has won
     * @param $board current board
     * @param $player current player
     * @return bool
     */
    static function winnerCheck($board, $player)
    {
        // Change the current board to matriz
        $board = Board::toMatriz($board);

        // The different counters for the sequences are initialized
        $rowCount = [1, 1, 1];
        $columnCount = null;
        $principalDiagonalCount = 0;
        $secundaryDiagonalCount = 0;

        // the matrix is ​​traversed with 2 fors
        for ($i = 0; $i < 3; $i++) {
            $columnCount = 1;
            for ($j = 0; $j < 3; $j++) {
                // The horizontal sequence is detected
                if ($j < 2 && $board[$i][$j] == $board[$i][$j + 1] && $board[$i][$j] == $player) {
                    $columnCount++;
                }

                // Due to the behavior and course of the cycles, it must be determined if when completing all the
                // columns of a row, there is a winner
                if ($columnCount == 3)
                    break;

                // The principal diagonal sequence is detected
                if ($i == $j && $board[$i][$j] == $player) {
                    $principalDiagonalCount++;
                }

                // The secundary diagonal sequence is detected
                if ($i + $j == 2 && $board[$i][$j] == $player) {
                    $secundaryDiagonalCount++;
                }

                // The vertical sequence is detected,
                // due to the behavior and course of the cycles, to determine the vertical sequences, an array must be
                // used to store each move per row
                if ($i < 2 && $board[$i][$j] == $board[$i + 1][$j] && $board[$i][$j] == $player) {
                    $rowCount[$j]++;
                }
            }

            // The counters are checked and if one is equal to 3, there is a winner
            if (in_array(3, $rowCount) || $columnCount == 3 ||  $principalDiagonalCount == 3 || $secundaryDiagonalCount == 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * Board Model (Entity) toMatriz
     * Transforms the array board into a matrix
     * @param $board current board
     * @return the formated board
     */
    static function toMatriz($board)
    {
        $newBoard = null;
        $k = 0;
        for ($i=0; $i < 3; $i++) {
            for ($j=0; $j < 3; $j++) {
                $newBoard[$i][$j] = $board[$k];
                $k++;
            }
        }

        return $newBoard;
    }

    /**
     * Board Model (Entity) tieCheck
     * @param  $board current board
     * @param  $winner current value of winner variable
     * @return String / Boolean
     */
    static function tieCheck($board, $winner)
    {
        // Check if there is tie
        if (!in_array(0, $board) && !$winner) {
            $winner = '-';
        }

        return $winner;
    }
}
