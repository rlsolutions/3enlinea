<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Board;

class HomeController extends Controller
{
    private $board = [];                 // Is the general board
    private $primaryFigure = null;       // Is the figure that player 1 has selected
    private $secundaryFigure = null;     // Is the figure that correspond to player 2
    private $player = null;              // Indicate which player is the turn
    private $gameKind = null;            // Indicate if the game is for 2 players or player vs pc
    private $saveMode = "";              // Correspond to if game has manual save or autosave
    private $winner = null;              // Flag to determinate if there is a winner

    /**
     * HomeController constructor.
     * Default constructor that initialize the global variables
     */
    function __construct()
    {
        $this->board = [
            "0" => 0, "1" => 0, "2" => 0,
            "3" => 0, "4" => 0, "5" => 0,
            "6" => 0, "7" => 0, "8" => 0,
        ];

        $this->primaryFigure = 'x';
        $this->secundaryFigure = 'o';
        $this->player = 1;
        $this->gameKind = "2";
        $this->saveMode = "manual";
    }

    /**
     * HomeController start
     * This method set the global variables with the data from welcome page
     * and redirect to the home view where start the game
     * @param Request $request
     * @return home view
     */
    function start(Request $request)
    {
        $this->primaryFigure = $request->primaryFigure;
        $this->secundaryFigure = $this->primaryFigure == 'x' ? 'o' : 'x';
        $this->gameKind = $request->gameKind;
        $this->saveMode = $request->saveMode;

        return view('home')->with([
            'board' => $this->board,
            "player" => $this->player,
            'primaryFigure' => $this->primaryFigure,
            'secundaryFigure' => $this->secundaryFigure,
            'gameKind' => $this->gameKind,
            'saveMode' => $this->saveMode
        ]);
    }

    /**
     * HomeController start
     * Here is the initial view where the start game
     * @return home view
     */
    public function index()
    {
        return view('home')->with([
            'board' => $this->board,
            "player" => $this->player,
            'primaryFigure' => $this->primaryFigure,
            'secundaryFigure' => $this->secundaryFigure,
            'gameKind' => $this->gameKind
        ]);
    }

    /**
     * HomeController update
     * @param Request $request
     * This method is called each time some board's cell is clicked by a player and
     * manage the general logic of game
     * @return home view
     */
    function update(Request $request)
    {
        // Set de variables
        $this->board = $request->cell;
        $this->player = $request->player;
        $this->primaryFigure = $request->primaryFigure;
        $this->secundaryFigure = $request->secundaryFigure;
        $this->gameKind = $request->gameKind;
        $this->saveMode = $request->saveMode;

        // Check if the player that was play is winner
        $this->winner = Board::winnerCheck($this->board, $this->player);

        // If the gameKind is 2, there are 2 players
        if ($this->gameKind == 2) {
            // Verify that there is no winner and then update the player's turn.
            if (!$this->winner) {
                $this->player = $request->player == 1 ? $this->player = 2 : $this->player = 1;
            }
        } else {
            // Verify that there is no winner and the pc play
            if (!$this->winner) {
                $this->player = 1;
                $this->board = $this->playPc($this->board);
            }
        }

        // Check if the game has autosave, update the player's turn and save
        if ($this->saveMode == "auto") {
            $request->merge(['player' => $this->player]);
            $request->merge(['cell' => $this->board]);
            $this->save($request);
        }

        $this->winner = Board::tieCheck($this->board, $this->winner);

        return view('home')->with([
            'board' => $this->board,
            "player" => $this->player,
            'primaryFigure' => $this->primaryFigure,
            'secundaryFigure' => $this->secundaryFigure,
            'gameKind' => $this->gameKind,
            'saveMode' => $this->saveMode,
            'winner' => $this->winner
        ]);
    }

    /**
     * HomeController save
     * This method save the current game
     * @param Request $request
     * @return home view
     */
    function save(Request $request)
    {
        // The board is formatted in a comma separated string
        $board = "";
        foreach ($request->cell as $key => $value) {
            $board = $board . $value . ",";
        }

        // Save the board
        Board::create([
            'board' => trim($board, ','),
            'player' => $request->player,
            'primary_figure' =>  $request->primaryFigure,
            'secundary_figure' => $request->secundaryFigure,
            'gameKind' => $request->gameKind,
            'saveMode' => $request->saveMode
        ]);

        return view('home')->with([
            'board' => $request->cell,
            "player" => $request->player,
            'primaryFigure' => $request->primaryFigure,
            'secundaryFigure' => $request->secundaryFigure,
            'gameKind' => $request->gameKind,
            'saveMode' => $request->saveMode
        ]);
    }

    /**
     * HomeController load
     * load the last board saved in the data base
     * @return home view
     */
    function load()
    {
        $dbBoard = Board::all()->last();

        return view('home')->with([
            'board' => explode(",", $dbBoard->board),
            "player" => $dbBoard->player,
            'primaryFigure' => $dbBoard->primary_figure,
            'secundaryFigure' => $dbBoard->secundary_figure,
            'gameKind' => $dbBoard->gameKind,
            'saveMode' => $dbBoard->saveMode
        ]);
    }

    /**
     * HomeController playPc
     *  Execute the random move of the pc
     * @param $board - Current board
     * @return $board with the pc's move
     */
    function playPc($board)
    {
        // Check if the board has empty cells
        $finished = false;
        while (!$finished && in_array(0, $board)) {
            $i = rand(0, 8);

            if ($board[$i] == 0) {
                $board[$i] = 2;
                $finished = true;
            }
        }

        // check if the pc has won
        $this->winner = Board::winnerCheck($board, 2);
        $this->player = $this->winner ? 2 : 1;
        $this->winner = Board::tieCheck($board, $this->winner);

        return $board;
    }
}
