## 3 En Linea

El tres en línea, también conocido como Ceros y Cruces, tres en raya (España, Ecuador y Bolivia), o la vieja (en Venezuela), es un juego tradicional de lápiz y papel entre dos jugadores: X y O, que marcan los espacios de un tablero de 3×3 alternadamente. Este proyecto es una versión web de este juego el cual permite tanto jugar a 2 jugadores, simular jugar contra una computadora, permite seleccionar la figura con la cual se desea jugar, un sistema de guardado manual y uno de guardado automático.

## Heramientas Utilizadas

Este proyecto fue desarrollado con el framework PHP [Laravel](https://laravel.com/), la lógica y jugabilidad fue desarrollada en PHP tanto para el Back como para el Front, posee JQuery unicamente para detectar eventos de click y accionar envíos de formulario, Bootstrap 4 para el uso de clases y estilos, HTML y CSS. y una base de datos MariaDB(Mysql).

## Alojado en un Repositorio

- El proyecto se encuentra alojado en un repositorio GIT en [Bitbucket](https://bitbucket.org/rlsolutions/3enlinea/src/master/).

## Instalación

Por simplicidad de instalación debido a que fue desarrollado para una prueba, ya posee las carpetas vendor y posee un archivo .env estandar, el proyecto fue entregado en un archivo .rar y un dump de la base de datos, sin embargo si se desea descargar desde el repositorio los pasos serian los siguientes:

- Descargar e instalar GIT (esto dependera del sistema operativo el cual se posea).
- Tener instalado un servidor de aplicaciones, para su desarrollo fue usado [Laragon](https://laragon.org/), pero es posible usar cualquier otro como [XAMPP](https://www.apachefriends.org/es/index.html).
- En una terminal a elección, situarse en la ruta donde se desea descargar el proyecto (Si es laragon en la carpeta www, xampp htdocs o la carpeta del servidor de aplicaciones a elección) y ejecutar el comando dependiendo de la form:
   * HTTPS: git clone https://bitbucket.org/rlsolutions/3enlinea.git  
- Crear una base de datos vacáa MariaDB (fork de MySQL) una base de datos con el nombre 3enlinea, para hacer esto primeramente se debe tener MariaDB instalado y luego se puede hacer mediante la terminal o támbien con algún manejador de bases de datos como HeydiSQL, WorkBench o el que de su agrado. 
- Situarce en la carpeta del proyecto y ejecutar el comando: php artisan migrate
- Opcional: si el servidor aplicaciones fue laragon, luego de arrancar todos los servicios, al proyecto se le podra acceder a travez del nombre del proyecto .test ejm: http://3enlinea.test como url en el navegador.
- Si es xampp la url por la cual acceder debe ser o asemejarse a esta http://localhost/3enlinea/public/

Si se tiene el .rar y el dump, basta con descomprimir el proyecto, situarlo en el servidor de aplicaciones que desee como en los pasos del metodo de instalación anterior y ejecutar en una terminal o manejador de base de datos el .sql de la base de datos.

## Sugerencias y Correcciones

Ante cualquier error, sugerencias, o comentario, por favor indicarme via correo a elluisluna@gmail.com, estare muy agradecido.  
