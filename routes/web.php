<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('welcome');
})->name('/');

Route::get('/start', 'HomeController@start')->name('start');
Route::post('/start', 'HomeController@start')->name('start');
Route::get('/home', 'HomeController@index')->name('home');
Route::put('/update', 'HomeController@update')->name('update');
Route::put('/save', 'HomeController@save')->name('save');
Route::put('/load', 'HomeController@load')->name('load');
